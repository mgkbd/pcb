The Noisy Cricket (Mini Gaming Keyboard)
========================================

![front](https://gitlab.com/noisy_cricket/pcb/-/raw/main/noisy_cricket_front.png?inline=false)
![back](https://gitlab.com/noisy_cricket/pcb/-/raw/main/noisy_cricket_back.png?inline=false)

BOM: https://octopart.com/bom-tool/6LG2ivlW

## Revision 1

Seems to have some bugs with the crystal, so don't use it.

## Revision 2

Seems to have fixed the bugs. Feel free to use!


---

Made with ❤️ by [XenGi](https://gitlab.com/XenGi).
